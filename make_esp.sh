#!/bin/bash
echo "This has to be run from inside the project directory"
action=$1
chip=$2
[ $# -eq 0 ] && { echo "Usage: $0 [flash|clean] [esp8266|esp32]"; exit 1; }

BASEPATH="/home/tdido/src/main/make/tools"
make -f $BASEPATH/makeEspArduino/makeEspArduino.mk ESP_ROOT=$BASEPATH/$chip CHIP=$chip $action
