#!/bin/bash
echo "This has to be run from inside the project directory"
action=$1
[ $# -eq 0 ] && { echo "Usage: $0 [flash|clean]"; exit 1; }

BASEPATH="/home/tdido/src/make/tools"
make -f $BASEPATH/makeEspArduino/makeEsp32Arduino.mk ESP_ROOT=$BASEPATH/esp32 $1
