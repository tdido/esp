#include <EEPROM.h>
#include <GxEPD.h>
#include <GxGDEP015OC1/GxGDEP015OC1.h>
#include <GxIO/GxIO_SPI/GxIO_SPI.h>
#include <GxIO/GxIO.h>
#include <Fonts/FreeMonoBold18pt7b.h>
#include <Fonts/FreeMonoBold12pt7b.h>
#include "DHT.h"

//ESP8266 (https://cdn.shopify.com/s/files/1/1509/1638/files/D1_Mini_Pinout_Diagram.pdf)
GxIO_Class io(SPI, /*CS=RX*/ 3, /*DC=D3*/ 0, /*RST=D4*/ 2); // D3(=0), D4(=2)
GxEPD_Class display(io, /*RST=D4*/ 2, /*BUSY=D2*/ 4); // D4(=2), D2(=4)

//ESP32
//GxIO_Class io(SPI, /*CS=5*/ 12, /*DC=*/ 17, /*RST=*/ 16); // arbitrary selection of 17, 16
//GxEPD_Class display(io, /*RST=*/ 16, /*BUSY=*/ 4); // arbitrary selection of (16), 4

#define DHTPIN 12
#define DHTTYPE DHT22
DHT dht(DHTPIN, DHTTYPE);

const int sleepSecs = 60;
const int tempAddr = 0;
const int humAddr = 4;

void drawData(float temp,float hum) {
    display.fillScreen(GxEPD_WHITE);

    display.setCursor(0, 55);

    display.println();
    display.print(F("T:"));
    display.print(temp);
    display.print(F("C"));

    display.println();
    display.print(F("H:"));
    display.print(hum);
    display.print(F("%"));

    display.update();
}

void drawMessage(String msg) {
    display.fillScreen(GxEPD_WHITE);
    display.setCursor(0, 30);
    display.println(msg);
    display.update();
}

bool checkBound(float newValue, float prevValue, float maxDiff) {
  return !isnan(newValue) &&
         (newValue < prevValue - maxDiff || newValue > prevValue + maxDiff);
}

void initDisplay(){
    display.init(115200);
    display.setRotation(3);
    display.setTextColor(GxEPD_BLACK);
}

void setup() {
    Serial.begin(115200);
    EEPROM.begin(512);
    dht.begin();

    float temp = 0;
    float hum = 0;
    float diff = 0.5;
    int update = 0;

    if(ESP.getResetReason() != "Deep-Sleep Wake"){
        initDisplay();
        display.setFont(&FreeMonoBold12pt7b);
        drawMessage("Giving you 15 seconds to flash before initialising...");
        delay(15000);
        update=1;
    }else{
        EEPROM.get(tempAddr, temp);
        delay(500);
        EEPROM.get(humAddr, hum);
        delay(500);
    }

    float newHum = dht.readHumidity();
    float newTemp = dht.readTemperature();

    if (checkBound(newTemp, temp, diff)) {
        EEPROM.put(tempAddr,newTemp);
        delay(500);
        update = 1;
    }

    if (checkBound(newHum, hum, diff)) {
        EEPROM.put(humAddr,newHum);
        delay(500);
        update = 1;
    }

    if(update){
        initDisplay();
        display.setFont(&FreeMonoBold18pt7b);
        drawData(newTemp,newHum);
        EEPROM.commit();
        delay(500);
    }
    ESP.deepSleep(sleepSecs * 1000000);
}

void loop() {
}

